<?php
namespace Martdb\Utils;

/**
 * 数字类型. 可用于明确数值序列化采用的类型.
 *
 * @author Chunlin Jing
 */
class TNumber
{
    public const INT = 4;
    public const LONG = 8;
    public const FLOAT = 16;
    public const DOUBLE = 32;
    
    private $number = 0;
    private $type = TNumber::INT;
    
    public function __construct($number, $type = TNumber::DOUBLE) {
        $this->number = $number;
        $this->type = $type;
    }
    
    public function getNumber() {
        return $this->number;
    }
    
    public function getType() {
        return $this->type;
    }
    
    // Override
    public function __toString() {
        return "number:".$this->number.", type:".$this->type;
    }
}

