<?php
namespace Martdb\Enums;


/**
 * 支持的数据类型
 *
 * @author Chunlin Jing
 */
class Types
{
    // Cannot use 'static' as constant modifier: public static const
    // PHP OOP is relatively weak
    public const BOOLEAN_FALSE = 0x00;
    public const BOOLEAN_TRUE  = 0x01;
    public const I32           = 0x20;
    public const I64           = 0x40;
    public const FLOAT         = 0x60;
    public const DOUBLE        = 0x70;
    public const STRING        = -0x80;
    public const BYTES         = -0x70;
    public const LIST          = -0x60;
    public const MAP           = -0x50;
    public const NULL          = -0x10;
}

