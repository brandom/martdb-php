# Laravel MartDB

** Laravel MartDB 使用文档 **
主包：[martdb-php](https://gitee.com/brandom/martdb-php "martdb-php")
依赖包：[thrift-php](https://gitee.com/brandom/thrift-php "thrift-php")




- **加载类包**
	切到项目跟路径下执行：`composer require martdb-php/martdb 1.0.0`
	
- **加载服务**
	切换到项目配置 `config/app.php` 找到**providers** 添加类`MartdbServiceProvider::class`
	
- **打包发布**
	切换到项目根目录下执行 `php artisan vendor:publish --provider="Martdb\MartdbServiceProvider" `, 这时生成该服务配置项`config/martdb.php`.
	




